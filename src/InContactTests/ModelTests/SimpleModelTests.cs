﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Helpers;
using InContactSdk.Models;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace InContactTests.ModelTests
{
    [ExcludeFromCodeCoverage]
    public class SimpleModelTests
    {
        [Fact]
        public void GenerateEndpoint_AccountInfo_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.AccountInfo;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "account/info?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_AddContacts_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.AddContacts;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "activities/addcontacts?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_ClearLists_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.ClearLists;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "activities/clearlists?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_ContactList_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.ContactList;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "lists?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_EmailCampaigns_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.EmailCampaigns;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "emailmarketing/campaigns?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_EventSpotEvents_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.EventSpotEvents;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "eventspot/events?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_ExportContacts_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.ExportContacts;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "activities/exportcontacts?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_Library_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.Library;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "library/info?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_LibraryFiles_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.LibraryFiles;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "library/files?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_LibraryFolders_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.LibraryFolders;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "library/folders?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_LibraryTrashFolder_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.LibraryTrashFolder;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "library/folders/trash/files?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_RemoveFromLists_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.RemoveFromLists;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "activities/removefromlists?api_key=" + settings.ApiKey, result);
        }

        [Fact]
        public void GenerateEndpoint_VerifiedEmailAddress_Returns_Correct_Endpoint()
        {
            // Arrange
            var serviceEndpoint = ServiceEndpoint.VerifiedEmailAddress;
            var settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            var simpleEndpoint = new SimpleModel(serviceEndpoint);

            // Act
            var result = simpleEndpoint.GenerateEndpoint(settings);

            // Assert
            Assert.Equal(settings.BaseUrl + "account/verifiedemailaddresses?api_key=" + settings.ApiKey, result);
        }
    }
}