﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.ContactLists;
using InContactSdk.ContactLists.Models;
using InContactSdk.Contacts;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.ContactListTests
{
    [ExcludeFromCodeCoverage]
    public class ContactListServiceTests
    {
        private ContactListsService contactListsService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public ContactListServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            webServiceRequest = new Mock<IWebServiceRequest>();
        }


        public void InitializeContactListsService<T>()
            where T : class, new()
        {
            contactListsService = new ContactListsService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetContactsList_ModifiedSince_Is_Null_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ContactList>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ContactList>());

            // Act
            await contactListsService.GetContactListsAsync(null);

            // Assert
            Assert.Equal(string.Format("{0}lists?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetContactsList_ModifiedSince_Not_Null_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var modifiedSince = DateTime.Now.AddMonths(-12);
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ContactList>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ContactList>());

            // Act
            await contactListsService.GetContactListsAsync(modifiedSince);

            // Assert
            Assert.Equal(string.Format("{0}lists?api_key={1}&modified_since={2}", settings.BaseUrl, settings.ApiKey, modifiedSince.ToString("o")), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetContactsList_Param_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var modifiedSince = DateTime.Now.AddMonths(-12);

            // Act
            await contactListsService.GetContactListsAsync(modifiedSince);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<ContactList>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateContactAsyncList_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var list = new ContactList
            {
                Name = "New List",
                Status = ContactListsStatus.Active
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ContactList, ContactList>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactList>()))
                .Callback((Uri a, string b, ContactList c) => endpoint = a)
                .ReturnsAsync(new ContactList());

            // Act
            await contactListsService.CreateContactListAsync(list);

            // Assert
            Assert.Equal(string.Format("{0}lists?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateContactAsyncList_Valid_List_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var list = new ContactList
            {
                Name = "New List",
                Status = ContactListsStatus.Active
            };

            // Act
            await contactListsService.CreateContactListAsync(list);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ContactList, ContactList>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactList>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetContactListModel_Uses_Correct_Endpoint()
        {
            // Arrange     
            InitializeContactListsService<ContactList>();
            string listId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ContactList>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ContactList());

            // Act
            await contactListsService.GetContactListAsync(listId);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}?api_key={2}", settings.BaseUrl, listId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetContactList_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            string listId = "123456";

            // Act
            await contactListsService.GetContactListAsync(listId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ContactList>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateContactList_Uses_Correct_Endpoint()
        {
            // Arrange     
            InitializeContactListsService<ContactList>();
            var listId = "123456";
            var list = new ContactList
            {
                Name = "New List",
                Status = ContactListsStatus.Active
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<ContactList, ContactList>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactList>()))
                .Callback((Uri a, string b, ContactList c) => endpoint = a)
                .ReturnsAsync(new ContactList());

            // Act
            await contactListsService.UpdateContactListAsync(listId, list);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}?api_key={2}", settings.BaseUrl, listId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateContactList_Valid_List_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var listId = "123456";
            var list = new ContactList
            {
                Name = "New List",
                Status = ContactListsStatus.Active
            };

            // Act
            await contactListsService.UpdateContactListAsync(listId, list);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<ContactList, ContactList>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactList>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteContactAsyncList_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var listId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await contactListsService.DeleteContactListAsync(listId);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}?api_key={2}", settings.BaseUrl, listId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteContactAsyncList_Verify_MakeRequestAsync()
        {
            // Arrange
            InitializeContactListsService<ContactList>();
            var listId = "123456";

            // Act
            await contactListsService.DeleteContactListAsync(listId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task ContactListMembershipModel_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<PaginatedResult<Contact>>();      
            var listId = "123456789";
            var limit = 50;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<Contact>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<Contact>());

            // Act
            await contactListsService.GetContactListMembershipAsync(listId, limit: limit);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}/contacts?api_key={2}", settings.BaseUrl, listId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task ContactListMembershipModel_Limit_Is_Not_50_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<PaginatedResult<Contact>>();            
            var listId = "123456789";
            var limit = 100;  
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<Contact>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<Contact>());

            // Act
            await contactListsService.GetContactListMembershipAsync(listId, limit: limit);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}/contacts?api_key={2}&limit={3}", settings.BaseUrl, listId, settings.ApiKey, limit), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task ContactListMembershipModel_ModifiedSince_Is_Not_Null_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<PaginatedResult<Contact>>();
            var listId = "123456789";
            var modifiedSince = new DateTime(2010, 1, 1);
            var limit = 50;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<Contact>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<Contact>());
            
            // Act
            await contactListsService.GetContactListMembershipAsync(listId, modifiedSince, limit);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}/contacts?api_key={2}&modified_since={3}", settings.BaseUrl, listId, settings.ApiKey, modifiedSince.ToString("o")), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task ContactListMembershipModel_Limit_Is_Not_50_ModifiedSince_Is_Not_Null_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactListsService<PaginatedResult<Contact>>();
            var listId = "123456789";
            var modifiedSince = new DateTime(2010, 1, 1);
            var limit = 100;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<Contact>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<Contact>());           

            // Act
            await contactListsService.GetContactListMembershipAsync(listId, modifiedSince, limit);

            // Assert
            Assert.Equal(string.Format("{0}lists/{1}/contacts?api_key={2}&limit={3}&modified_since={4}", settings.BaseUrl, listId, settings.ApiKey, limit, modifiedSince.ToString("o")), endpoint.AbsoluteUri);                        
        }

        [Fact]
        public async Task GetContactListMembership_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactListsService<PaginatedResult<Contact>>();
            string listId = "123456";

            // Act
            await contactListsService.GetContactListMembershipAsync(listId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<Contact>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }
    }
}