﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaignScheduleService.Models;
using InContactSdk.EmailCampaignScheduling.Validation;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.EmailCampaignSchedulingTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class CreateEmailCampaignScheduleModelValidatorTests
    {
        private readonly CreateEmailCampaignScheduleModelValidator validator;

        public CreateEmailCampaignScheduleModelValidatorTests()
        {
            validator = new CreateEmailCampaignScheduleModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void CampaignId_Is_NullOrWhiteSpace_Generates_Errors()
        {
            // Arrange
            var campaignId = "";
            var scheduleDate = DateTime.Now;

            var model = new CreateEmailCampaignScheduleModel { CampaignId = campaignId, ScheduledDate = scheduleDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "campaignId is required"));
        }

        [Fact]
        public void CampaignId_NullOrWhiteSpace_Generates_Errors()
        {
            // Arrange
            var campaignId = "123456";
            var scheduleDate = DateTime.Now;

            var model = new CreateEmailCampaignScheduleModel { CampaignId = campaignId, ScheduledDate = scheduleDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "campaignId is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void ScheuduledDate_Is_In_The_Past_Generates_Error()
        {
            // Arrange
            var campaignId = "123456";
            var scheduleDate = DateTime.Now.AddDays(-1);

            var model = new CreateEmailCampaignScheduleModel { CampaignId = campaignId, ScheduledDate = scheduleDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "scheduledDate must be in the future"));
        }

        [Fact]
        public void ScheuduledDate_Is_Before_1_1_1970_Generates_Error()
        {
            // Arrange
            var campaignId = "123456";
            var scheduleDate = new DateTime();

            var model = new CreateEmailCampaignScheduleModel { CampaignId = campaignId, ScheduledDate = scheduleDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "scheduledDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void ScheduledDate_Is_After_1_1_1970_Is_Valid()
        {
            // Arrange
            var campaignId = "123456";
            var scheduleDate = DateTime.Now.AddDays(1);

            var model = new CreateEmailCampaignScheduleModel { CampaignId = campaignId, ScheduledDate = scheduleDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "scheduledDate must be in the future must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }
    }
}