﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaignScheduleService.Models;
using InContactSdk.EmailCampaignScheduleService.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.EmailCampaignSchedulingTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignScheduleModelValidatorTests
    {
        private readonly EmailCampaignScheduleModelValidator validator;

        public EmailCampaignScheduleModelValidatorTests()
        {
            validator = new EmailCampaignScheduleModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void CampaignId_Is_NullOrWhiteSpace_GeneratesErrors()
        {
            // Act
            var campaignId = "";
            var scheduleId = "";

            var model = new EmailCampaignScheduleModel { CampaignId = campaignId, ScheduleId = scheduleId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "campaignId is required"));
        }

        [Fact]
        public void CampaignId_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Act
            var campaignId = "123456";
            var scheduleId = "";

            var model = new EmailCampaignScheduleModel { CampaignId = campaignId, ScheduleId = scheduleId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "campaignId is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void ScheduleId_Is_NullOrWhiteSpace_GeneratesErrors()
        {
            // Act
            var campaignId = "";
            var scheduleId = "";

            var model = new EmailCampaignScheduleModel { CampaignId = campaignId, ScheduleId = scheduleId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "scheduleId is required"));
        }

        [Fact]
        public void ScheduleId_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Act
            var campaignId = "";
            var scheduleId = "123456";

            var model = new EmailCampaignScheduleModel { CampaignId = campaignId, ScheduleId = scheduleId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "scheduleId is required"));
        }
    }
}