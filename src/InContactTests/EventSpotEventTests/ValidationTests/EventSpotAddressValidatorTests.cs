﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EventSpotEvents;
using InContactSdk.EventSpotEvents.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EventSpotAddressValidatorTests
    {
        private readonly EventSpotAddressValidator validator;

        public EventSpotAddressValidatorTests()
        {
            validator = new EventSpotAddressValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Line1_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string line1 = "very absurdly super long address that is not valid.";
            var model = new EventSpotAddress { Line1 = line1 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("Line1 cannot be greater than 50 characters")));
        }

        [Fact]
        public void Line1_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string line1 = "Valid Address";
            var model = new EventSpotAddress { Line1 = line1 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("Line1 cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void Line2_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string line2 = "very absurdly super long address that is not valid.";
            var model = new EventSpotAddress { Line2 = line2 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("Line2 cannot be greater than 50 characters")));
        }

        [Fact]
        public void Line2_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string line2 = "Valid Address";
            var model = new EventSpotAddress { Line2 = line2 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("Line2 cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void Line3_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string line3 = "very absurdly super long address that is not valid.";
            var model = new EventSpotAddress { Line3 = line3 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("Line3 cannot be greater than 50 characters")));
        }

        [Fact]
        public void Line3_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string line3 = "Valid Address";
            var model = new EventSpotAddress { Line3 = line3 };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("Line3 cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void City_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string city = "very absurdly super long city name that is not valid.";
            var model = new EventSpotAddress { City = city };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("City cannot be greater than 50 characters")));
        }

        [Fact]
        public void City_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string city = "Pasadena";
            var model = new EventSpotAddress { City = city };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("City cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string state = "very absurdly super long state name that is not valid.";
            var model = new EventSpotAddress { State = state };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("State cannot be greater than 50 characters")));
        }

        [Fact]
        public void State_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string state = "California";
            var model = new EventSpotAddress { State = state };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("State cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void StateCode_Greater_Than_50_Characters_GeneratesError()
        {
            // Arrange
            string stateCode = "very absurdly super long state code that is not valid.";
            var model = new EventSpotAddress { StateCode = stateCode };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("StateCode cannot be greater than 50 characters")));
        }

        [Fact]
        public void StateCode_Less_Than_50_Characters_Is_Valid()
        {
            // Arrange
            string stateCode = "CA";
            var model = new EventSpotAddress { StateCode = stateCode };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("StateCode cannot be greater than 50 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void PostalCode_Greater_Than_25_Characters_GeneratesError()
        {
            // Arrange
            string postalCode = "super long invalid postal code that exists no where";
            var model = new EventSpotAddress { PostalCode = postalCode };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("PostalCode cannot be greater than 25 characters")));
        }

        [Fact]
        public void PostalCode_Less_Than_25_Characters_Is_Valid()
        {
            // Arrange
            string postalCode = "91101";
            var model = new EventSpotAddress { PostalCode = postalCode };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("PostalCode cannot be greater than 25 characters")));
        }

        // *************************************************************************************************

        [Fact]
        public void Country_Greater_Than_128_Characters_GeneratesError()
        {
            // Arrange
            string country = "super long invalid nonexistant country name that does not exist anywhere on this earth, in spacetime or in this multiverse... or maybe it does";
            var model = new EventSpotAddress { Country = country };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage.Contains("Country cannot be greater than 128 characters")));
        }

        [Fact]
        public void Country_Less_Than_128_Characters_Is_Valid()
        {
            // Arrange
            string country = "United States of America";
            var model = new EventSpotAddress { Country = country };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage.Contains("Country cannot be greater than 128 characters")));
        }
    }
}