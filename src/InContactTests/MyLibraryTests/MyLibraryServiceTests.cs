﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.MyLibrary;
using InContactSdk.MyLibrary.Models;
using InContactSdk.Validation;
using Moq;
using Xunit;

namespace InContactTests.MyLibraryTests
{
    [ExcludeFromCodeCoverage]
    public class MyLibraryServiceTests
    {
        private readonly IInContactSettings settings = null;
        private readonly Mock<IValidationService> validationService = null;
        private readonly Mock<IWebServiceRequest> webServiceRequest = null;
        private readonly MyLibraryService service;

        public MyLibraryServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            webServiceRequest = new Mock<IWebServiceRequest>();
            service = new MyLibraryService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFilesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<LibraryFile>());

            // Act
            await service.GetLibraryFilesAsync(50, LibrarySortBy.CreatedDateDesc, LibrarySource.All, LibraryType.All);

            // Assert
            Assert.Equal(string.Format("{0}library/files?limit=50&sort_by=CREATED_DATE_DESC&source=ALL&type=ALL&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFilesAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFilesAsync(50, LibrarySortBy.CreatedDateDesc, LibrarySource.All, LibraryType.All);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFilesByFolderAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<LibraryFile>());

            // Act
            await service.GetLibraryFilesByFolderAsync("-2");

            // Assert
            Assert.Equal(string.Format("{0}library/folders/-2/files?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFilesByFolderAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFilesByFolderAsync("-2");

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFileAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<LibraryFile>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new LibraryFile());

            // Act
            await service.GetLibraryFileAsync("1");

            // Assert
            Assert.Equal(string.Format("{0}library/files/1?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFileAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFileAsync("1");

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<LibraryFile>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateLibraryFileAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<UpdateLibraryFileModel, LibraryFile>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<UpdateLibraryFileModel>()))
                .Callback((Uri a, string b, UpdateLibraryFileModel c) => endpoint = a)
                .ReturnsAsync(new LibraryFile());

            // Act
            await service.UpdateLibraryFileAsync("1", null, "Balloon.png", "description", SdkBoolean.True);

            // Assert
            Assert.Equal(string.Format("{0}library/files/1?include_payload=TRUE&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateLibraryFileAsync_Verify_PutDeserializedAsync()
        {
            // Act
            await service.UpdateLibraryFileAsync("1", null, "Balloon.png", "description", SdkBoolean.True);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<UpdateLibraryFileModel, LibraryFile>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<UpdateLibraryFileModel>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteLibraryFilesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteLibraryFilesAsync(new string[] { "1", "4" });

            // Assert
            Assert.Equal(string.Format("{0}library/files/1,4?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteLibraryFilesAsync_Verify_DeleteAsync()
        {
            // Act
            await service.DeleteLibraryFilesAsync(new string[] { "1" });

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task AddLibraryFileAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostMultipartAsync(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()))
                .Callback((Uri a, string b, IEnumerable<KeyValuePair<string, object>> c) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            var str = "I'm not insane. My mother had me tested!";
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            // Act
            await service.AddLibraryFileAsync("fileName.jpg", "12", "description", LibrarySource.All, LibraryFileType.Jpg, bytes);

            // Assert
            Assert.Equal(string.Format("{0}library/files?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task AddLibraryFileAsync_UsesCorrectPostParameterKeys()
        {
            // Arrange
            IEnumerable<KeyValuePair<string, object>> postParameters = null;

            webServiceRequest.Setup(m => m.PostMultipartAsync(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()))
                .Callback((Uri a, string b, IEnumerable<KeyValuePair<string, object>> c) => postParameters = c)
                .ReturnsAsync(new HttpResponseMessage());

            var str = "I'm not insane. My mother had me tested!";
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            // Act
            await service.AddLibraryFileAsync("fileName.jpg", "12", "description", LibrarySource.All, LibraryFileType.Jpg, bytes);

            // Assert
            Assert.Equal(6, postParameters.Count());
            Assert.True(postParameters.Where(x => x.Key == "file_name").Any());
            Assert.True(postParameters.Where(x => x.Key == "folder_id").Any());
            Assert.True(postParameters.Where(x => x.Key == "description").Any());
            Assert.True(postParameters.Where(x => x.Key == "source").Any());
            Assert.True(postParameters.Where(x => x.Key == "file_type").Any());
            Assert.True(postParameters.Where(x => x.Key == "data").Any());
        }

        [Fact]
        public async Task AddLibraryFileAsync_IfResponseHeaderLocationIsNull_ReturnsNull()
        {
            // Arrange
            var response = new HttpResponseMessage();

            webServiceRequest.Setup(m => m.PostMultipartAsync(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()))
                .ReturnsAsync(response);
            var str = "I'm not insane. My mother had me tested!";
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            // Act
            var result = await service.AddLibraryFileAsync("fileName.jpg", "12", "description", LibrarySource.All, LibraryFileType.Jpg, bytes);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task AddLibraryFileAsync_Verify_PostMultipartAsync()
        {
            // Arrange
            var response = new HttpResponseMessage();

            response.Headers.Add("Location", "http://www.example.com");

            webServiceRequest.Setup(m => m.PostMultipartAsync(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()))
                .ReturnsAsync(response);

            var str = "I'm not insane. My mother had me tested!";
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            // Act
            await service.AddLibraryFileAsync("fileName.jpg", "12", "description", LibrarySource.All, LibraryFileType.Jpg, bytes);

            // Assert
            webServiceRequest.Verify(m => m.PostMultipartAsync(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFilesStatusAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<LibraryFileStatus>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<LibraryFileStatus>());

            // Act
            await service.GetLibraryFilesStatusAsync(new string[] { "1", "3" });

            // Assert
            Assert.Equal(string.Format("{0}library/files/uploadstatus/1,3?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFilesStatusAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFilesStatusAsync(new string[] { "1" });

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<LibraryFileStatus>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task MoveLibraryFilesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<IEnumerable<string>, IEnumerable<LibraryFileMoveResult>>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<string>>()))
                .Callback((Uri a, string b, IEnumerable<string> c) => endpoint = a)
                .ReturnsAsync(new List<LibraryFileMoveResult>());

            // Act
            await service.MoveLibraryFilesAsync(new string[] { "1", "3" }, "12");

            // Assert
            Assert.Equal(string.Format("{0}library/folders/12/files?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task MoveLibraryFilesAsync_Verify_PutDeserializedAsync()
        {
            // Act
            await service.MoveLibraryFilesAsync(new string[] { "1" }, "12");

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<IEnumerable<string>, IEnumerable<LibraryFileMoveResult>>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<string>>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibrarySummaryInformationAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<LibrarySummaryInformation>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new LibrarySummaryInformation());

            // Act
            await service.GetLibrarySummaryInformationAsync();

            // Assert
            Assert.Equal(string.Format("{0}library/info?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibrarySummaryInformationAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibrarySummaryInformationAsync();

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<LibrarySummaryInformation>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFolderCollectionAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<LibraryFolder>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<LibraryFolder>());

            // Act
            await service.GetLibraryFolderCollectionAsync(50, LibrarySortBy.CreatedDateDesc);

            // Assert
            Assert.Equal(string.Format("{0}library/folders?limit=50&sort_by=CREATED_DATE_DESC&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFolderCollectionAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFolderCollectionAsync(50, LibrarySortBy.CreatedDateDesc);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<LibraryFolder>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateLibraryFolderAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<CreateLibraryFolderModel, LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<CreateLibraryFolderModel>()))
                .Callback((Uri a, string b, CreateLibraryFolderModel c) => endpoint = a)
                .ReturnsAsync(new LibraryFolder());

            // Act
            await service.CreateLibraryFolderAsync("name", "11");

            // Assert
            Assert.Equal(string.Format("{0}library/folders?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateLibraryFolderAsync_Verify_PostDeserializedAsync()
        {
            // Act
            await service.CreateLibraryFolderAsync("name", "11");

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<CreateLibraryFolderModel, LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<CreateLibraryFolderModel>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetLibraryFolderAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new LibraryFolder());

            // Act
            await service.GetLibraryFolderAsync("11");

            // Assert
            Assert.Equal(string.Format("{0}library/folders/11?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetLibraryFolderAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetLibraryFolderAsync("11");

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateLibraryFolderAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<UpdateLibraryFolderModel, LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<UpdateLibraryFolderModel>()))
                .Callback((Uri a, string b, UpdateLibraryFolderModel c) => endpoint = a)
                .ReturnsAsync(new LibraryFolder());

            // Act
            await service.UpdateLibraryFolderAsync("12", "foo", "11", SdkBoolean.True);

            // Assert
            Assert.Equal(string.Format("{0}library/folders/12?include_payload=TRUE&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateLibraryFolderAsync_Verify_PutDeserializedAsync()
        {
            // Act
            await service.UpdateLibraryFolderAsync("12", "foo", "11", SdkBoolean.True);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<UpdateLibraryFolderModel, LibraryFolder>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<UpdateLibraryFolderModel>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteLibraryFolderAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteLibraryFolderAsync("11");

            // Assert
            Assert.Equal(string.Format("{0}library/folders/11?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteLibraryFolderAsync_Verify_DeleteAsync()
        {
            // Act
            await service.DeleteLibraryFolderAsync("11");

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetTrashFilesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<LibraryFile>());

            // Act
            await service.GetTrashFilesAsync(50, LibrarySortBy.CreatedDateDesc, LibraryType.All);

            // Assert
            Assert.Equal(string.Format("{0}library/folders/trash/files?limit=50&sort_by=CREATED_DATE_DESC&type=ALL&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetTrashFilesAsync_Verify_GetDeserializedAsync()
        {
            // Act
            await service.GetTrashFilesAsync(50, LibrarySortBy.CreatedDateDesc, LibraryType.All);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<LibraryFile>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteTrashFilesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteTrashFilesAsync();

            // Assert
            Assert.Equal(string.Format("{0}library/folders/trash/files?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteTrashFilesAsync_Verify_DeleteAsync()
        {
            // Act
            await service.DeleteTrashFilesAsync();

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

    }
}