﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentValidation.TestHelper;
using InContactSdk.MyLibrary.Models;
using InContactSdk.MyLibrary.Validation;
using Xunit;

namespace InContactTests.MyLibraryTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class AddLibraryFileModelValidatorTests
    {
        private readonly AddLibraryFileModelValidator validator;

        public AddLibraryFileModelValidatorTests()
        {
            validator = new AddLibraryFileModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void FileName_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = null,
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "   ",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfLengthIs5_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = string.Join("", Enumerable.Range(1, 5).Select(x => "a")),
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfLengthIsLessThan5_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = string.Join("", Enumerable.Range(1, 4).Select(x => "a")),
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfLengthIs80_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = string.Join("", Enumerable.Range(1, 80).Select(x => "a")),
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfLengthIsGreaterThan80_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = string.Join("", Enumerable.Range(1, 81).Select(x => "a")),
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileName, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileName_IfEmpty_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = null,
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify a file name.").Any());
        }

        [Fact]
        public void FileName_IfTooLong_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = string.Join("", Enumerable.Range(1, 81).Select(x => "a")),
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "The max file name length is 80 characters.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void FolderId_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = null,
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "   ",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FolderId, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FolderId_IfEmpty_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = null,
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify the destination folder. If there are no folders in the account, set to 0.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void Description_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = null,
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Description, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Description_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Description, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Description_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "   ",
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Description, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Description_IfLengthIs100_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = string.Join("", Enumerable.Range(1, 100).Select(x => "a")),
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.Description, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Description_IfLengthIsGreaterThan100_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = string.Join("", Enumerable.Range(1, 101).Select(x => "a")),
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Description, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Description_IfEmpty_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = null,
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify a description.").Any());
        }

        [Fact]
        public void Description_IfTooLong_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = string.Join("", Enumerable.Range(1, 101).Select(x => "a")),
                Source = "source",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "The max description length is 100 characters.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void Source_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = null,
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Source, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Source_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Source, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Source_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "   ",
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Source, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Source_IfEmpty_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = null,
                FileType = "filetype",
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify the source.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void FileType_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = null,
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileType, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileType_IfEmpty_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileType, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileType_IfWhiteSpace_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "   ",
                Data = new byte[0]
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.FileType, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void FileType_IfEmpty_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = null,
                Data = new byte[0]
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify the file type.").Any());
        }

        // *************************************************************************************************

        [Fact]
        public void Data_IfNull_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = null
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.Data, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void Data_IfNull_AddsError()
        {
            // Arrange
            var model = new AddLibraryFileModel()
            {
                FileName = "filename",
                FolderId = "12",
                Description = "description",
                Source = "source",
                FileType = "filetype",
                Data = null
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must supply a file to add.").Any());
        }

        // *************************************************************************************************

    }
}