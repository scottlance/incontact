﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts;
using InContactSdk.Contacts.Validation;
using InContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.ContactsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ContactEmailAddressValidatorTests
    {
        private readonly ContactEmailAddressValidator validator;

        public ContactEmailAddressValidatorTests()
        {
            validator = new ContactEmailAddressValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Email_Is_Null_Throws_Error()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress();

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddress is required"));
        }

        [Fact]
        public void Email_Is_Empty_Throws_Error()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress()
            {
                EmailAddress = ""
            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddress is required"));
        }

        [Fact]
        public void Email_Is_Not_Valid_Throws_Error()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress()
            {
                EmailAddress = "scooper@caltech"
            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddress is not valid"));
        }

        [Fact]
        public void Email_Is_Longer_Than_80_Characters_Throws_Error()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress()
            {
                EmailAddress = "scooper@caltechwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.com"
            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddress cannot contain more than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Opt_In_Source_Is_None_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptInSource = OptInSource.None

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert            
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Opt_In_Source_Is_ACTION_BY_VISITOR_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptInSource = OptInSource.ActionByVisitor

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Opt_In_Source_Is_ACTION_BY_OWNER_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptInSource = OptInSource.ActionByOwner

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Opt_In_Source_Is_ACTION_BY_SYSTEM_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptInSource = OptInSource.ActionBySystem

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        // *************************************************************************************************

        [Fact]
        public void Opt_Out_Source_Is_Empty_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptOutSource = OptOutSource.None

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Opt_Out_Source_Is_ACTION_BY_VISITOR_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptOutSource = OptOutSource.ActionByVisitor

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }

        [Fact]
        public void Opt_Out_Source_Is_ACTION_BY_OWNER_Is_Valid()
        {
            // Arrange
            var contactEmailAddress = new ContactEmailAddress
            {
                EmailAddress = "scooper@caltech.edu",
                OptOutSource = OptOutSource.ActionByOwner

            };

            // Act
            var result = validator.Validate(contactEmailAddress);

            // Assert
            Assert.Equal(0, result.Errors.Count);
        }
    }
}