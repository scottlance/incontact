﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts.Models;
using InContactSdk.Contacts.Validation;
using InContactSdk.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.ContactsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class GetAllContactsModelValidatorTests
    {
        private readonly GetAllContactsModelValidator validator;

        public GetAllContactsModelValidatorTests()
        {
            validator = new GetAllContactsModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Email_Is_Null_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 50, ModifiedSince = null, Email = "" };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Email is invalid"));
        }

        [Fact]
        public void Email_Is_Invalid_Email_Generates_Error()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 50, ModifiedSince = null, Email = "scooper@caltech" };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Email is invalid"));
        }

        // *************************************************************************************************

        [Fact]
        public void Status_Is_Null_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 50, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Status must be blank or one of the following: ALL, ACTIVE, UNCONFIRMED, OPTOUT, REMOVED"));
        }

        // *************************************************************************************************

        [Fact]
        public void Limit_Is_0_Generates_Error()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 0, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_501_Generates_Error()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 501, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_42_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 42, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Limit must be between 1 and 500"));
        }

        // *************************************************************************************************

        [Fact]
        public void Modified_Date_Is_Null_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 1, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ModifiedSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Modified_Date_Is_Empty_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 1, ModifiedSince = null, Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ModifiedSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Modified_Date_Is_Formatted_Correctly_Is_Valid()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 1, ModifiedSince = new DateTime(2015, 3, 11, 3, 30, 0).ToString("o"), Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ModifiedSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Modified_Date_Is_Formatted_Correctly_Date_Is_Before_1_1_1970_Generates_Error()
        {
            // Arrange
            var getAllContactsModel = new GetAllContactsModel { Status = ContactStatusQuery.All, Limit = 1, ModifiedSince = new DateTime(1969, 12, 31, 0, 0, 0).ToString("o"), Email = null };

            // Act
            var result = validator.Validate(getAllContactsModel);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ModifiedSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }
    }
}