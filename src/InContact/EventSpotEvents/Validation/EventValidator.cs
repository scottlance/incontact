﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.EventSpotEvents;
using InContactSdk.Helpers;
using System.Linq;

namespace InContactSdk.Validation
{
    internal class EventValidator : AbstractValidator<Event>
    {
        public EventValidator()
        {
            RuleFor(m => m.Address).NotNull().When(m => m.IsMapDisplayed == true).WithMessage("Address must be entered when IsMapDisplayed is true");
            RuleFor(m => m.Contact).NotNull().WithMessage("Contact is required");
            RuleFor(m => m.Description).Length(0, 350).WithMessage("Description cannot be greater than 500 characters");
            RuleFor(m => m.EndDate).NotNull().WithMessage("EndDate is required").NotEmpty().WithMessage("EndDate is required").ISO8601Date().WithMessage("EndDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
            RuleFor(m => m.GoogleAnalyticsKey).Length(0, 20).WithMessage("GoogleAnalyticsKey cannot be greater than 20 characters");
            RuleFor(m => m.GoogleMerchantId).Length(0, 20).WithMessage("GoogleMerchantId cannot be greater than 20 characters");
            RuleFor(m => m.Location).NotNull().WithMessage("Location is required").NotEmpty().WithMessage("Location is required").Length(0, 50).WithMessage("Location cannot be greater than 50 characters");
            RuleFor(m => m.MetaDataTags).Length(0, 100).WithMessage("MetaDataTags cannot be greater than 100 characters");
            RuleFor(m => m.Name).NotNull().WithMessage("Name is required").NotEmpty().WithMessage("Name is required").Length(0, 100).WithMessage("Name cannot be greater than 100 characters");
            RuleFor(m => m.OnlineMeeting).NotNull().When(m => m.IsVirtualEvent).WithMessage("OnlineMeeting is required when IsVirtualEvent is set to true");
            RuleFor(m => m.OnlineMeeting).Must(m => m != null && m.All(u => !string.IsNullOrWhiteSpace(u.Url))).When(m => m.IsVirtualEvent).WithMessage("OnlineMeeting.Url is required when IsVirtualMeeting is set to true");
            RuleFor(m => m.PayableTo).Length(0, 128).WithMessage("PayableTo cannot be greater than 128 characters");
            RuleFor(m => m.PayableTo).NotEmpty().When(m => m.PaymentOptions != null && m.PaymentOptions.Any(p => p.Equals(PaymentType.Check))).WithMessage("PayableTo is required when PaymentOptions contains the Check payment type");
            RuleFor(m => m.PaymentAddress).NotNull().When(m => m.PaymentOptions != null && m.PaymentOptions.Any(p => p.Equals(PaymentType.Check))).WithMessage("PaymentAddress is required when PaymentOptions contains the Check payment type");
            RuleFor(m => m.PayPalAccountEmail).Length(0, 128).WithMessage("PayPalAccountEmail cannot be greater than 128 characters");
            RuleFor(m => m.PayPalAccountEmail).NotEmpty().When(m => m.PaymentOptions != null && m.PaymentOptions.Any(p => p == PaymentType.PayPal)).WithMessage("PayPalAccountEmail is required when PaymentOptions contains the PayPal payment type");
            RuleFor(m => m.StartDate).NotNull().WithMessage("StartDate is required").NotEmpty().WithMessage("StartDate is required").ISO8601Date().WithMessage("StartDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
            RuleFor(m => m.TimeZoneDescription).Length(0, 80).WithMessage("TimeZoneDescription cannot be greater than 80 characters");
            RuleFor(m => m.Title).NotNull().WithMessage("Title is required").NotEmpty().WithMessage("Title is required").Length(0, 100).WithMessage("Title cannot be longer than 100 characters");
            RuleFor(m => m.TwitterHashTag).Length(0, 30).WithMessage("TwitterHashTag cannot be greater than 30 characters");
        }
    }
}