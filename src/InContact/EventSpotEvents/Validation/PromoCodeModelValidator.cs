﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.Helpers;
using System.Linq;

namespace InContactSdk.EventSpotEvents.Validation
{
    internal class PromoCodeValidator<T> : AbstractValidator<T>
        where T : PromoCode
    {
        public PromoCodeValidator()
        {
            RuleFor(m => m.CodeName).NotNull().WithMessage("CodeName is required").NotEmpty().WithMessage("CodeName is required").Matches(@"^(([A-Za-z]|[0-9]){4,12})$").WithMessage("CodeName must be between 4 and 12 characters and cannot contain spaces or special characters");        
            RuleFor(m => m.FeeIds).Must(m => m != null && m.Count() == 1).When(m => m.CodeType == PromoCodeType.Access).WithMessage("FeeIds must contain only one fee id when CodeType is ACCESS");
            RuleFor(m => m.DiscountScope).Must(m => m == PromoCodeDiscountScope.FeeList || m == PromoCodeDiscountScope.OrderTotal).When(m => m.CodeType == PromoCodeType.Discount).WithMessage("DiscountScope is required when CodeType is DISCOUNT");
            RuleFor(m => m.QuantityTotal).Must(m => m == -1 || m > 0).WithMessage("QuantityTotal must be a positive number or -1, where -1 is unlimited");
        }
    }

    internal class PromoCodeDiscountAmountValidator : PromoCodeValidator<DiscountAmountPromoCode>
    {
        public PromoCodeDiscountAmountValidator()
        {
            RuleFor(m => m.DiscountAmount).Must(m => m > 0).WithMessage("DiscountAmount must be greater than 0.00");
        }
    }

    internal class PromoCodeDiscountPercentValidator : PromoCodeValidator<DiscountPercentPromoCode>
    {
        public PromoCodeDiscountPercentValidator()
        {
            RuleFor(m => m.DiscountPercent).InclusiveBetween(1, 100).WithMessage("DiscountPercent must be between 1 and 100");
        }
    }
}