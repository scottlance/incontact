﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InContactSdk.Contacts.Models
{
    internal class UpdateContactModel : IValidatable
    {
        public IInContactSettings Settings { get; set; }
        public Contact Contact { get; set; }
        public string ContactId { get; set; }
        public ActionBy ActionBy { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            var errors = new List<ValidationError>();
            errors.AddRange(Validator.Validate<UpdateContactModel, UpdateContactModelValidator>(this));
            errors.AddRange(this.Contact.Validate());

            if (this.Contact.EmailAddresses != null)
            {
                if (EnumStrings.OptInSourceStrings[this.Contact.EmailAddresses.First().OptInSource] != EnumStrings.ActionByStrings[this.ActionBy])
                    errors.Add(new ValidationError("OptInSource", "Contact.EmailAddresses[0].OptInSource cannot be different than ActionBy"));

                if (EnumStrings.OptInSourceStrings[this.Contact.EmailAddresses.First().OptInSource] == "ACTION_BY_SYSTEM")
                    errors.Add(new ValidationError("Contact.EmailAddresses[0].OptInSource", "OptInSource cannot be ACTION_BY_SYSTEM on Contact Update"));
            }

            return errors;
        }

        public string GenerateEndpoint()
        {
            var endpoint = new StringBuilder(string.Format("{0}{1}/{2}?api_key={3}", this.Settings.BaseUrl, EnumStrings.ServiceEndpointStrings[ServiceEndpoint.Contacts], this.ContactId, this.Settings.ApiKey));

            if (this.ActionBy != ActionBy.None)
                endpoint.AppendFormat("&action_by={0}", EnumStrings.ActionByStrings[this.ActionBy]);

            return endpoint.ToString();
        }
    }
}