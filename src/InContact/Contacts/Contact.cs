﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using InContactSdk.Contacts.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Validation;
using Newtonsoft.Json;

namespace InContactSdk.Contacts
{
    [JsonObject("results")]
    public class Contact : IValidatable
    {
        [JsonProperty("addresses")]
        public IEnumerable<Address> Addresses { get; set; }
        [JsonProperty("cell_phone")]
        public string CellPhone { get; set; }
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }
        [JsonProperty("confirmed")]
        public bool? Confirmed { get; set; }
        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }
        [JsonProperty("custom_fields")]
        public IEnumerable<CustomField> CustomFields { get; set; }
        [JsonProperty("email_addresses")]
        public IEnumerable<ContactEmailAddress> EmailAddresses { get; set; }
        [JsonProperty("fax")]
        public string Fax { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("home_phone")]
        public string HomePhone { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("job_title")]
        public string JobTitle { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("lists")]
        public IEnumerable<List> Lists { get; set; }
        [JsonProperty("modified_date")]
        public string ModifiedDate { get; internal set; }
        [JsonProperty("notes")]
        public IEnumerable<ContactNote> Notes { get; set; }
        [JsonProperty("prefix_name")]
        public string PrefixName { get; set; }
        [JsonProperty("source")]
        public string Source { get; internal set; }
        [JsonProperty("source_details")]
        public string SourceDetails { get; internal set; }
        [JsonProperty("status"), JsonConverter(typeof(TypeEnumConverter<ContactStatus, BiLookup<ContactStatus, string>>))]
        public ContactStatus Status { get; internal set; }
        [JsonProperty("work_phone")]
        public string WorkPhone { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            var errors = new List<ValidationError>();
            errors.AddRange(Validator.Validate<Contact, ContactValidator>(this));

            if (this.Addresses != null)
                foreach (var a in this.Addresses)
                    errors.AddRange(a.Validate());

            if (this.CustomFields != null)
                foreach (var c in this.CustomFields)
                    errors.AddRange(c.Validate());

            if (this.EmailAddresses != null)
                foreach (var e in this.EmailAddresses)
                    errors.AddRange(e.Validate());

            if (this.Lists != null)
                foreach (var l in this.Lists)
                    errors.AddRange(l.Validate());

            if (this.Notes != null)
                foreach (var n in this.Notes)
                    errors.AddRange(n.Validate());

            return errors;
        }
    }
}