﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.Contacts.Models;

namespace InContactSdk.Contacts.Validation
{
    internal class UpdateContactModelValidator : AbstractValidator<UpdateContactModel>
    {
        public UpdateContactModelValidator()
        {
            RuleFor(m => m.ContactId).NotEmpty().WithMessage("ContactId is required");

            // This is a weird one, the documentation (http://developer.constantcontact.com/docs/contacts-api/contacts-resource.html?method=PUT)
            // suggests that the note can be updated, but when I try to update a contact with a note, I get an error in the response stating 
            // [{"error_key":"json.field.note.too.many","error_message":"Only one note is allowed"}].  This suggests that you cannot update a 
            // note in the PUT request.  
            //
            // The only way to correctly validate this is to do a GET request on the contact, check to see if a note exists and then use that 
            // information to validate the PUT request.  That really isn't really feasable in a one request scenario (like this API) and this 
            // seems to be a bug with the API so I am just going to restrict notes being uploaded in the PUT request for now, until I hear back
            // from the web services team on my bug report
            RuleFor(m => m.Contact.Notes).Must(n => n == null).WithMessage("Notes cannot be included in the Contact Update");
        }
    }
}