﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace InContactSdk.EmailCampaigns.Validation
{
    internal class MessageFooterValidator : AbstractValidator<MessageFooter>
    {
        public MessageFooterValidator()
        {
            RuleFor(m => m.AddressLine1).Length(0, 50).WithMessage("AddressLine1 must not exceed 50 characters");
            RuleFor(m => m.AddressLine2).Length(0, 50).WithMessage("AddressLine2 must not exceed 50 characters");
            RuleFor(m => m.AddressLine3).Length(0, 50).WithMessage("AddressLine3 must not exceed 50 characters");

            // Some weirdness here and in some of these other chained validators with the .When() where length wont be validated,
            // so break them into two different rules.
            RuleFor(m => m.ForwardEmailLinkText).Length(0, 45).WithMessage("ForwardEmailLinkText must not exceed 45 characters");
            RuleFor(m => m.ForwardEmailLinkText).NotEmpty().When(m => m.IncludeForwardEmail).WithMessage("ForwardEmailLinkText is required when IncludeForwardEmail is true");
            RuleFor(m => m.InternationalState).Length(0, 50).WithMessage("InternationalState must not exceed 50 characters");
            RuleFor(m => m.InternationalState).NotEmpty().When(m => m.Country != "US" && !string.IsNullOrWhiteSpace(m.Country)).WithMessage("InternationalState is required if the Country is not US");
            RuleFor(m => m.OrganizationName).Length(0, 50).WithMessage("OrganizationName must not exceed 50 characters");
            RuleFor(m => m.PostalCode).Length(0, 25).WithMessage("PostalCode must not exceed 25 characters");
            RuleFor(m => m.State).Length(0, 2).WithMessage("State must not exceed 2 characters");
            RuleFor(m => m.State).NotEmpty().When(m => m.Country != null && m.Country.ToUpperInvariant().Equals("US")).WithMessage("State is required when Country is US");
            RuleFor(m => m.SubscribeLinkText).Length(0, 45).WithMessage("SubscribeLinkText must not exceed 45 characters");
            RuleFor(m => m.SubscribeLinkText).NotEmpty().When(m => m.InlcudeSubscribeLink).WithMessage("SubscribeLinkText is required when IncludeSubscribeLink is true");
        }
    }
}