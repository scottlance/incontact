﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.MyLibrary.Models;
using InContactSdk.Validation;

namespace InContactSdk.MyLibrary
{
    public class MyLibraryService : ServiceBase, IMyLibraryService
    {
        public MyLibraryService(IInContactSettings settings)
            : base(settings)
        { }

        internal MyLibraryService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-collections.html?method=GET
        public async Task<PaginatedResult<LibraryFile>> GetLibraryFilesAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc, LibrarySource source = LibrarySource.All, LibraryType type = LibraryType.All)
        {
            var model = new GetLibraryFilesModel() { QueryLimit = limit, QuerySortBy = sortBy, QuerySource = source, QueryType = type };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<LibraryFile>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-collection-by-folder.html?method=GET
        public async Task<PaginatedResult<LibraryFile>> GetLibraryFilesByFolderAsync(string folderId)
        {
            var model = new GetLibraryFilesByFolderModel() { FolderId = folderId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<LibraryFile>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-resource-api.html?method=GET
        public async Task<LibraryFile> GetLibraryFileAsync(string fileId)
        {
            var model = new GetLibraryFileModel() { FileId = fileId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<LibraryFile>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-resource-api.html?method=PUT
        public async Task<LibraryFile> UpdateLibraryFileAsync(string fileId, string folderId, string name, string description, SdkBoolean includePayload)
        {
            var model = new UpdateLibraryFileModel() { FileId = fileId, FolderId = folderId, Name = name, Description = description, IncludePayload = includePayload };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<UpdateLibraryFileModel, LibraryFile>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, model);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-resource-api.html?method=DELETE
        public async Task DeleteLibraryFilesAsync(IEnumerable<string> fileIds)
        {
            var model = new DeleteLibraryFileModel() { FileIds = fileIds };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-add-multipart-post.html?method=POST
        public async Task<string> AddLibraryFileAsync(string fileName, string folderId, string description, LibrarySource source, LibraryFileType fileType, byte[] data)
        {
            var model = new AddLibraryFileModel() { FileName = fileName, FolderId = folderId, Description = description, Source = EnumStrings.LibrarySourceStrings[source], FileType = EnumStrings.LibraryFileTypeStrings[fileType], Data = data };
            validationService.Validate(model);

            var postParameters = new List<KeyValuePair<string, object>>() 
            { 
                new KeyValuePair<string, object>("file_name", model.FileName),
                new KeyValuePair<string, object>("folder_id", model.FolderId),
                new KeyValuePair<string, object>("description", model.Description),
                new KeyValuePair<string, object>("source", model.Source),
                new KeyValuePair<string, object>("file_type", model.FileType),
                new KeyValuePair<string, object>("data", model.Data)
            };

            var result = await webServiceRequest.PostMultipartAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, postParameters);

            if (result.Headers.Location == null)
                return null;

            return result.Headers.Location.Segments.Last();
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/file-upload-status.html?method=GET
        public async Task<IEnumerable<LibraryFileStatus>> GetLibraryFilesStatusAsync(IEnumerable<string> fileIds)
        {
            var model = new GetLibraryFileStatusModel() { FileIds = fileIds };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<IEnumerable<LibraryFileStatus>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-files-api/move-a-file.html?method=PUT
        public async Task<IEnumerable<LibraryFileMoveResult>> MoveLibraryFilesAsync(IEnumerable<string> fileIds, string destinationFolderId)
        {
            var model = new MoveLibraryFileModel() { FileIds = fileIds, FolderId = destinationFolderId };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<IEnumerable<string>, IEnumerable<LibraryFileMoveResult>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, model.FileIds);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-info.html?method=GET
        public async Task<LibrarySummaryInformation> GetLibrarySummaryInformationAsync()
        {
            var endpoint = new SimpleModel(ServiceEndpoint.Library).GenerateEndpoint(settings);
            return await webServiceRequest.GetDeserializedAsync<LibrarySummaryInformation>(new Uri(endpoint), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-folder-collection.html?method=GET
        public async Task<PaginatedResult<LibraryFolder>> GetLibraryFolderCollectionAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc)
        {
            var model = new GetLibraryFolderCollectionModel() { QueryLimit = limit };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<LibraryFolder>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-folder-collection.html?method=POST
        public async Task<LibraryFolder> CreateLibraryFolderAsync(string name, string parentFolderId)
        {
            var model = new CreateLibraryFolderModel() { Name = name, ParentId = parentFolderId };
            validationService.Validate(model);

            return await webServiceRequest.PostDeserializedAsync<CreateLibraryFolderModel, LibraryFolder>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, model);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-individual-folder.html?method=GET
        public async Task<LibraryFolder> GetLibraryFolderAsync(string folderId)
        {
            var model = new GetLibraryFolderModel() { FolderId = folderId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<LibraryFolder>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-individual-folder.html?method=PUT
        public async Task<LibraryFolder> UpdateLibraryFolderAsync(string folderId, string name, string parentId, SdkBoolean includePayload)
        {
            var model = new UpdateLibraryFolderModel() { FolderId = folderId, Name = name, ParentId = parentId ?? "0", IncludePayload = includePayload };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<UpdateLibraryFolderModel, LibraryFolder>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, model);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/mylibrary-individual-folder.html?method=DELETE
        public async Task DeleteLibraryFolderAsync(string folderId)
        {
            var model = new DeleteLibraryFolderModel() { FolderId = folderId };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/trash-folder.html?method=GET
        public async Task<PaginatedResult<LibraryFile>> GetTrashFilesAsync(int limit = 50, LibrarySortBy sortBy = LibrarySortBy.CreatedDateDesc, LibraryType type = LibraryType.All)
        {
            var model = new GetTrashFilesModel() { QueryLimit = limit, QuerySortBy = sortBy, QueryType = type };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<LibraryFile>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/mylibrary-folders-api/trash-folder.html?method=DELETE
        public async Task DeleteTrashFilesAsync()
        {
            var endpoint = new SimpleModel(ServiceEndpoint.LibraryTrashFolder).GenerateEndpoint(settings);
            await webServiceRequest.DeleteAsync(new Uri(endpoint), settings.AuthToken);
        }
    }
}