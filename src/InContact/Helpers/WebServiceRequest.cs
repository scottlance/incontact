﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Exceptions;
using InContactSdk.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace InContactSdk.Helpers
{
    internal class WebServiceRequest : IWebServiceRequest
    {
        private readonly ISerialization serialization;

        public WebServiceRequest(ISerialization serialization)
        {
            this.serialization = serialization;
        }

        public async Task<HttpResponseMessage> GetAsync(Uri requestUri, string accessToken)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<T> GetDeserializedAsync<T>(Uri requestUri, string accessToken) where T : class
        {
            var result = await GetAsync(requestUri, accessToken);
            var content = await result.Content.ReadAsStringAsync();

            return serialization.Deserialize<T>(content);
        }

        public async Task<HttpResponseMessage> PutAsync<T>(Uri requestUri, string accessToken, T payload) where T : class
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Put, requestUri);
            var content = new StringContent(serialization.Serialize(payload));

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            requestMessage.Content = content;

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<T2> PutDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload)
            where T1 : class
            where T2 : class
        {
            var result = await PutAsync<T1>(requestUri, accessToken, payload);
            var content = await result.Content.ReadAsStringAsync();

            return serialization.Deserialize<T2>(content);
        }

        public async Task<HttpResponseMessage> DeleteAsync(Uri requestUri, string accessToken)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, requestUri);

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<HttpResponseMessage> PatchAsync<T>(Uri requestUri, string accessToken, T payload) where T : class
        {
            var requestMessage = new HttpRequestMessage(new HttpMethod("PATCH"), requestUri);
            var content = new StringContent(serialization.Serialize(payload));

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            requestMessage.Content = content;

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<T2> PatchDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload)
            where T1 : class
            where T2 : class
        {
            var result = await PatchAsync<T1>(requestUri, accessToken, payload);
            var content = await result.Content.ReadAsStringAsync();

            return serialization.Deserialize<T2>(content);
        }

        public async Task<HttpResponseMessage> PostAsync<T>(Uri requestUri, string accessToken, T payload) where T : class
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri);
            var content = new StringContent(serialization.Serialize(payload));

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            requestMessage.Content = content;

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<T2> PostDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload)
            where T1 : class
            where T2 : class
        {
            var result = await PostAsync<T1>(requestUri, accessToken, payload);
            var content = await result.Content.ReadAsStringAsync();

            return serialization.Deserialize<T2>(content);
        }

        public async Task<HttpResponseMessage> PostMultipartAsync(Uri requestUri, string accessToken, IEnumerable<KeyValuePair<string, object>> postParameters)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri);
            var multiPartContent = new MultipartFormDataContent(string.Format("----------{0:N}", Guid.NewGuid()));

            foreach (var obj in postParameters)
            {
                HttpContent content = null;

                if (obj.Value is string)
                {
                    content = new StringContent(obj.Value as string);
                }
                else if (obj.Value is byte[])
                {
                    content = new ByteArrayContent(obj.Value as byte[]);
                    content.Headers.Add("Content-Type", "application/octet-stream");
                }

                if (content != null)
                    multiPartContent.Add(content, string.Format("\"{0}\"", obj.Key));
            }

            requestMessage.Headers.ExpectContinue = false;
            requestMessage.Content = multiPartContent;

            return await MakeRequestAsync(requestMessage, accessToken);
        }

        public async Task<T> PostMultiparttDeserializedAsync<T>(Uri requestUri, string accessToken, IEnumerable<KeyValuePair<string, object>> postParameters) where T : class
        {
            var result = await PostMultipartAsync(requestUri, accessToken, postParameters);
            var content = await result.Content.ReadAsStringAsync();

            return serialization.Deserialize<T>(content);
        }

        private async Task<HttpResponseMessage> MakeRequestAsync(HttpRequestMessage request, string accessToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", accessToken));
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                //return await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
                var result = await httpClient.SendAsync(request);

                if (result.IsSuccessStatusCode)
                    return result;

                var content = await result.Content.ReadAsStringAsync();
                var error = serialization.Deserialize<IEnumerable<EndpointError>>(content);

                throw new InContactEndpointException(error);
            }
        }

    }
}