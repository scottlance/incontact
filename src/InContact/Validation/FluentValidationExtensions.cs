﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation.Validators;
using InContactSdk.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FluentValidation
{
    internal static class FluentValidationExtensions
    {
        public static IRuleBuilderOptions<T, string> Empty<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new Null<string>());
        }

        public static IRuleBuilderOptions<T, IEnumerable<ExportColumnName>> ExportMustContainEmail<T>(this IRuleBuilder<T, IEnumerable<ExportColumnName>> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new ExportMustContainEmail<IEnumerable<ExportColumnName>>());
        }

        public static IRuleBuilderOptions<T, TElement> ISO8601Date<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsIso8601Date<TElement>());
        }

        public static IRuleBuilderOptions<T, IEnumerable<string>> ListsFormattedCorrectly<T>(this IRuleBuilder<T, IEnumerable<string>> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new ListsFormattedCorrectly<IEnumerable<string>>());
        }

        public static IRuleBuilderOptions<T, string> MustBeUpperCase<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new MustBeUpperCase<string>());
        }

        public static IRuleBuilderOptions<T, IEnumerable<ImportColumnName>> ImportMustContainEmail<T>(this IRuleBuilder<T, IEnumerable<ImportColumnName>> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new ImportMustContainEmail<IEnumerable<ImportColumnName>>());
        }

        public static IRuleBuilderOptions<T, IEnumerable<TElement>> NotEmptyList<T, TElement>(this IRuleBuilder<T, IEnumerable<TElement>> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new NotEmptyList<TElement>());
        }

        public static IRuleBuilderOptions<T, TElement> Null<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new Null<TElement>());
        }
    }

    internal class Empty<T> : PropertyValidator
    {
        public Empty()
            : base("{Property Name} must be empty")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue as string;

            if (string.IsNullOrWhiteSpace(property))
                return true;

            return false;
        }
    }

    internal class ExportMustContainEmail<T> : PropertyValidator
    {
        public ExportMustContainEmail()
            : base("{Property Name} must contain email")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue as IEnumerable<ExportColumnName>;

            if (property == null)
                return false;
           
            return (property ).Contains(ExportColumnName.Email);
        }
    }

    internal class IsIso8601Date<T> : PropertyValidator
    {
        public IsIso8601Date()
            : base("{Property Name} must be a in a ISO 8601 format and must be a date greater than 1/1/1970")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue;

            if (property == null)
                return true;

            if (property.GetType() == typeof(string))
            {
                var stringProperty = property as string;

                if (stringProperty == null || string.IsNullOrWhiteSpace(stringProperty))
                    return true;

                var regex = new Regex(@"^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$");

                if (!regex.IsMatch(stringProperty))
                    return false;
                try
                {
                    DateTime result;
                    DateTime.TryParse(stringProperty, out result);

                    if (result >= new DateTime(1970, 1, 1, 0, 0, 0))
                        return true;
                }
                catch (Exception)
                { }
            }
            else if (property.GetType() == typeof(DateTime))
            {
                return Convert.ToDateTime(property) >= new DateTime(1970, 1, 1, 0, 0, 0) ? true : false;
            }

            return false;
        }
    }

    internal class ListsFormattedCorrectly<T> : PropertyValidator
    {
        public ListsFormattedCorrectly()
            : base("{Property Name} a correctly formated list of lists")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var lists = context.PropertyValue as IEnumerable<string>;

            if (lists == null)
                return true;

            foreach (var l in lists)
            {
                Int64 output;
                if (!Int64.TryParse(l, out output))
                    return false;
            }

            return true;
        }
    }

    internal class MustBeUpperCase<T> : PropertyValidator
    {
        public MustBeUpperCase()
            : base("{Property Name} must be upper case")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue as string;

            if (property == null || string.IsNullOrWhiteSpace(property))
                return true;

            return property.ToUpperInvariant().Equals(property);
        }
    }

    internal class ImportMustContainEmail<T> : PropertyValidator
    {
        public ImportMustContainEmail()
            : base("{Property Name} must contain email")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue as IEnumerable<ImportColumnName>;

            if (property == null)
                return false;
           
            return (property ).Contains(ImportColumnName.Email);
        }
    }

    internal class NotEmptyList<T> : PropertyValidator
    {
        public NotEmptyList()
            : base("{Property Name} not be an empty list")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue as IEnumerable<T>;

            if (property == null || property.Count() == 0)
                return false;

            return true;
        }
    }

    internal class Null<T> : PropertyValidator
    {
        public Null()
            : base("{Property Name} must be null")
        { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var property = context.PropertyValue;

            if (property == null)
                return true;

            return false;
        }
    }
}