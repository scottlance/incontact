﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using System.Linq;

namespace InContactSdk.Account.Validation
{
    internal class AccountSummaryValidator : AbstractValidator<AccountSummary>
    {
        public AccountSummaryValidator()
        {
            RuleFor(a => a.Email).EmailAddress().WithMessage("Email is not a valid email address").Length(0, 80).WithMessage("Email cannot contain more than 80 characters");
            RuleFor(a => a.FirstName).Length(0, 80).WithMessage("FirstName cannot contain more than 80 characters");
            RuleFor(a => a.LastName).Length(0, 80).WithMessage("LastName cannot contain more than 80 characters");
            RuleFor(a => a.OrganizationAddresses).Must(oa => oa.Count() == 1).When(a => a.OrganizationAddresses != null).WithMessage("organization_address can only contain one element");
            RuleFor(a => a.OrganizationName).Length(0, 80).WithMessage("OrganizationName cannot contain more than 80 characters");
            RuleFor(a => a.Phone).Length(0, 20).WithMessage("Phone cannot contain more than 20 characters");
            RuleFor(a => a.StateCode).Length(2).When(s => s.StateCode != null).WithMessage("StateCode must be exactly 2 characters");
            RuleFor(a => a.Website).Length(0, 255).WithMessage("Website cannot contain more than 255 characters");
        }
    }
}